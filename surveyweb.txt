FROM ubuntu:18.10
MAINTAINER vietic

RUN apt-get clean && apt-get -y update && apt-get install -y apt-utils locales
ENV LANG=en_US.UTF-8 LANGUAGE=en_US:en LC_CTYPE="en_US.UTF-8" LC_NUMERIC=en_US.UTF-8 LC_TIME=en_US.UTF-8 LC_COLLATE="en_US.UTF-8" LC_MONETARY=en_US.UTF-8 LC_MESSAGES="en_US.UTF-8" LC_PAPER=en_US.UTF-8 LC_NAME=en_US.UTF-8 LC_ADDRESS=en_US.UTF-8 LC_TELEPHONE=en_US.UTF-8 LC_MEASUREMENT=en_US.UTF-8 LC_IDENTIFICATION=en_US.UTF-8 LC_ALL=en_US.UTF-8 LD_LIBRARY_PATH=/home/vietic/myspace/myworks:/home/vietic/myspace/surveyweb:/usr/local/lib:. APP_CONFIG=./resources/configs/app_config.conf PATH=/home/vietic/myspace/surveyweb:$PATH DEBIAN_FRONTEND=noninteractive

RUN locale-gen en_US.UTF-8 && apt-get update && apt-get install -y sudo zsh mc nano htop build-essential cmake git autoconf yasm nasm curl wget checkinstall rsync ssh openssh-server openssh-client openssl libssl-dev libiodbc2 libiodbc2-dev libmysqlclient-dev software-properties-common libxext-dev libxrender-dev libxtst-dev rabbitmq-server automake pkg-config libtool libffi-dev libgmp-dev libmysqlcppconn-dev freetds-dev freetds-bin libgtk2.0-dev libmicrohttpd-dev libboost-dev libcurl4-openssl-dev nginx tzdata redis-server

RUN mkdir -p /home/vietic/myspace/myworks && echo "root:123$%^" | chpasswd

# ADD ./surveyweb.tar.gz ./surveyangular.tar.gz /home/vietic/myspace/
# COPY ./surveyweb_nginx.conf /etc/nginx/sites-enabled/
# RUN chown -R www-data:www-data /home/vietic/myspace/surveyweb && chown -R www-data:www-data /home/vietic/myspace/surveyangular

RUN apt-get install -y python3-dev python3-pip python3-mysqldb python3-tk && pip3 install --upgrade pip Cython && pip install --upgrade pandas requests sklearn scikit-learn boto3 pika h5py psutil numpy scipy regex spacy future joblib Flask Flask-HTTPAuth flask_cors gunicorn unidecode redis six cryptography Jinja2 Werkzeug MarkupSafe pycryptodomex colored peewee pytz click itsdangerous guizero uwsgi pymssql

WORKDIR /home/vietic/myspace/surveyweb
# CMD ["/home/vietic/myspace/surveyweb/start.sh"]

########################################################################################################
# sudo docker build -t tnaga/vietic:survey00 . --force-rm=true
# docker run -it --rm -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix -v /home/tnaga/myspace/myworks:/home/vietic/myspace/myworks -v /etc/localtime:/etc/localtime:ro --device /dev/snd --device /dev/dri --device /dev/video0 tnaga/vietic:pythart01 /bin/bash
########################################################################################################

FROM tnaga/vietic:survey00
MAINTAINER vietic

ADD ./surveyweb.tar.gz ./surveyangular.tar.gz /home/vietic/myspace/
COPY ./surveyweb_nginx.conf /etc/nginx/sites-enabled/
RUN chown -R www-data:www-data /home/vietic/myspace/surveyweb && chown -R www-data:www-data /home/vietic/myspace/surveyangular

WORKDIR /home/vietic/myspace/surveyweb
CMD ["/home/vietic/myspace/surveyweb/start.sh"]
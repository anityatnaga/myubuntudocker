FROM ubuntu:22.04
MAINTAINER vietic

RUN apt-get clean && apt-get -y update && apt-get install -y apt-utils locales
ENV LANG=en_US.UTF-8 LANGUAGE=en_US:en LC_CTYPE="en_US.UTF-8" LC_NUMERIC=en_US.UTF-8 LC_TIME=en_US.UTF-8 LC_COLLATE="en_US.UTF-8" LC_MONETARY=en_US.UTF-8 LC_MESSAGES="en_US.UTF-8" LC_PAPER=en_US.UTF-8 LC_NAME=en_US.UTF-8 LC_ADDRESS=en_US.UTF-8 LC_TELEPHONE=en_US.UTF-8 LC_MEASUREMENT=en_US.UTF-8 LC_IDENTIFICATION=en_US.UTF-8 LC_ALL=en_US.UTF-8 LD_LIBRARY_PATH=/home/vietic/myspace/jdk-10/lib:/home/vietic/myspace/instantclient_19_3:/home/vietic/myspace/myworks:/home/vietic/myspace/lib:/usr/local/lib:/usr/lib:/lib/x86_64-linux-gnu:/usr/lib/x86_64-linux-gnu:. APP_CONFIG=./resources/configs/app_config.conf DEBIAN_FRONTEND=noninteractive PATH=/home/vietic/myspace/jdk-10/bin:/home/vietic/myspace/instantclient_19_3:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:.

RUN locale-gen en_US.UTF-8 && apt-get update && apt-get install -y sudo tzdata mc nano vim iputils-ping htop build-essential cmake git autoconf yasm curl wget checkinstall rsync ssh openssh-server openssh-client openssl libssl-dev software-properties-common libxext-dev libxrender-dev libxtst-dev automake pkg-config libtool libffi-dev libgmp-dev libgtk2.0-dev libmicrohttpd-dev libboost-dev libcurl4-openssl-dev nginx freetds-dev freetds-bin redis-server libtool libc6 libc6-dev unzip libnuma1 libnuma-dev zlib1g-dev libaio1 libx264-dev libx265-dev libmp3lame-dev libfdk-aac-dev libopus-dev libmysqlclient-dev libmysqlcppconn-dev unixodbc-dev unixodbc uuid-dev && mkdir -p /home/vietic/myspace/myworks && mkdir -p /home/vietic/myspace/lib && echo "root:123456" | chpasswd

RUN apt-get install -y python3-dev python3-pip python3-mysqldb python3-tk && pip3 install --upgrade pip Cython && pip install --upgrade pandas requests sklearn scikit-learn scikit-image boto3 pika h5py psutil numpy scipy regex spacy future joblib Flask Flask-HTTPAuth flask_cors gunicorn unidecode redis six cryptography Jinja2 Werkzeug MarkupSafe pycryptodomex colored peewee pytz click itsdangerous guizero uwsgi zeep cherrypy cx_Oracle pymssql

WORKDIR /home/vietic/myspace

ADD ./jdk-10_linux-x64_bin.tar.gz /home/vietic/myspace/

CMD ["nginx", "-g", "daemon off;"]

########################################################################################################
# sudo docker build -t tnaga/vietic:nginx.00 . --force-rm=true

FROM tnaga/myubuntu:tnagai04
MAINTAINER tnaga

ADD ./pa_new_180918.tar.gz /home/tnaga/myspace/tnaga-ai
WORKDIR /home/tnaga/myspace/tnaga-ai/pa
CMD ["./start.sh"]

########################################################################################################
# sudo docker build -t tnaga/myubuntu:panew01 . --force-rm=true
# docker run -itd --name nlp --rm -p 9010:9010 -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix -v /home/tnaga/myspace/myworks:/home/tnaga/myspace/myworks -v /etc/localtime:/etc/localtime:ro tnaga/myubuntu:nlp07

FROM tnaga/vietic:vcppbase05
MAINTAINER tnaga

ADD ./eva181210.tar.gz /home/vietic/myspace/myworks/
WORKDIR /home/vietic/myspace/myworks/eva_server
# CMD ["nginx", "-g", "daemon off;"]
CMD ["/home/vietic/myspace/myworks/eva_server/start.sh"]

########################################################################################################
# sudo docker build -t tnaga/vietic:evas01 . --force-rm=true
# docker run -itd --name nlp --rm -p 9010:9010 -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix -v /home/tnaga/myspace/myworks:/home/tnaga/myspace/myworks -v /etc/localtime:/etc/localtime:ro tnaga/myubuntu:nlp07

sudo apt-get install -y openssh-server
sudo curl -fsSL get.docker.com | sh
sudo usermod -aG docker $USER
sudo apt-get install -y mc nano build-essential cmake git autoconf curl wget checkinstall openssl libssl-dev libiodbc2 libiodbc2-dev libmysqlclient-dev software-properties-common libgtk2.0-dev pkg-config libmicrohttpd-dev libboost-dev libcurl4-openssl-dev yasm libxext-dev libxrender-dev libxtst-dev rabbitmq-server automake libtool libffi-dev libgmp-dev nginx
sudo apt-get install -y python3-dev python3-pip python3-mysqldb && sudo -H pip3 install --upgrade pip Cython setuptools && sudo -H pip install --upgrade keras nltk pandas requests sklearn bottle tensorflow cherrypy h5py pyvi psutil numpy scipy fasttext python-crfsuite underthesea pika
# sudo pip install pyethapp

mkdir -p ~/myspace/myworks
cd ~/myspace/myworks

git clone https://github.com/ethereum/pyethereum/
cd pyethereum
sudo -H pip3 install -r dev_requirements.txt
sudo -H pip3 install -r requirements.txt
sudo python3 setup.py install

cd ..
git clone https://github.com/ethereum/pyethapp
cd pyethapp
sudo -H pip3 install -r dev_requirements.txt
sudo -H pip3 install -r requirements.txt
sudo USE_PYETHEREUM_DEVELOP=1 python3 setup.py install

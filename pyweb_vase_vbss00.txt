FROM tnaga/vietic:pycpp04_nocuda
MAINTAINER vietic

ENV LD_LIBRARY_PATH=/home/vietic/myspace/myworks:/home/vietic/myspace/pyvbss:$LD_LIBRARY_PATH PATH=/home/vietic/myspace/pyvbss:$PATH

ADD ./pyvbss_built_38_200707b.tar.gz /home/vietic/myspace/
RUN chown -R www-data:www-data /home/vietic/myspace/pyvbss

WORKDIR /home/vietic/myspace/pyvbss
CMD ["/home/vietic/myspace/pyvbss/start.sh"]

########################################################################################################
# sudo docker build -t tnaga/vietic:pycpp04_nocuda_vbss01 . --force-rm=true
########################################################################################################

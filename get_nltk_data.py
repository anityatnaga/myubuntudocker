#!/usr/bin/python3
# -*- coding: utf8 -*-
import nltk
nltk.download('maxent_treebank_pos_tagger')
nltk.download('averaged_perceptron_tagger')
nltk.download('wordnet')
nltk.download('punkt')

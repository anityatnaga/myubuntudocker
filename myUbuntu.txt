FROM ubuntu:latest
MAINTAINER tnaga

RUN useradd -m vietic && adduser vietic sudo && chsh -s /bin/bash vietic
#RUN passwd vietic

# Set the timezone.
#RUN sudo echo "Asia/Ho_Chi_Minh" > /etc/timezone
#RUN sudo dpkg-reconfigure -f noninteractive tzdata
#ENV TZ=Asia/Ho_Chi_Minh
#RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

#RUN dd if=/dev/zero of=/swapfile bs=2048 count=1024k
#RUN mkswap /swapfile && swapon /swapfile
#RUN chown root:root /swapfile && chmod 0600 /swapfile
#RUN echo "/swapfile none swap sw 0 0" >> /etc/fstab

# Set the locale
#RUN locale-gen en_US en_US.UTF-8
#RUN dpkg-reconfigure locales
RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

RUN apt-get update && apt-get install -y sudo && apt-get install -y mc
RUN apt-get install -y nano curl wget && apt-get install -y build-essential checkinstall
RUN mkdir /home/vietic/myspace && mkdir /home/vietic/myspace/myworks
RUN chown -R vietic:vietic /home/vietic

#RUN echo "root:Docker!" | chpasswd

#USER tnaga
########################################################################################################
# sudo docker build -t tnaga/myubuntu:vietic .
# sudo docker run -i -t -p 8000:8000 -v ~/CongViec/myspace:/home/tnaga/myspace/myworks -u tnaga tnaga/myubuntu:v161227 /bin/bash

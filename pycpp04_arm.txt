FROM arm32v7/ubuntu:20.04
MAINTAINER vietic

# RUN apt-get clean && apt-get -y update && apt-get install -y apt-utils locales
ENV LANG=en_US.UTF-8 LANGUAGE=en_US:en LC_CTYPE="en_US.UTF-8" LC_NUMERIC=en_US.UTF-8 LC_TIME=en_US.UTF-8 LC_COLLATE="en_US.UTF-8" LC_MONETARY=en_US.UTF-8 LC_MESSAGES="en_US.UTF-8" LC_PAPER=en_US.UTF-8 LC_NAME=en_US.UTF-8 LC_ADDRESS=en_US.UTF-8 LC_TELEPHONE=en_US.UTF-8 LC_MEASUREMENT=en_US.UTF-8 LC_IDENTIFICATION=en_US.UTF-8 LC_ALL=en_US.UTF-8 LD_LIBRARY_PATH=/home/vietic/myspace/instantclient_19_3:/home/vietic/myspace/myworks:/usr/local/lib:. APP_CONFIG=./resources/configs/app_config.conf JAVA_HOME=/home/vietic/myspace/jdk1.8.0_202 PATH=$PATH:/home/vietic/myspace/jdk1.8.0_202/bin:/home/vietic/myspace/instantclient_19_3 DEBIAN_FRONTEND=noninteractive

RUN mkdir -p /home/vietic/myspace/myworks && echo "root:123$%^" | chpasswd

RUN apt-get -y update && apt-get install -y sudo zsh mc nano vim htop iputils-ping build-essential cmake git autoconf yasm curl wget checkinstall rsync ssh openssh-server openssh-client openssl libssl-dev software-properties-common libxext-dev libxrender-dev libxtst-dev rabbitmq-server automake pkg-config libtool libffi-dev libgmp-dev freetds-dev freetds-bin libgtk2.0-dev libmicrohttpd-dev libboost-dev libcurl4-openssl-dev nginx tzdata redis-server mpg123 libusb-1.0-0-dev libpcsclite-dev libsdl1.2-dev libsdl-ttf2.0-dev libaio1 unixodbc-dev unixodbc seccomp libc6 libc6-dev unzip libnuma1 libnuma-dev zlib1g-dev libaio1 libx264-dev libx265-dev libmp3lame-dev libfdk-aac-dev libopus-dev mysql-client libmysqlclient-dev libmysqlcppconn-dev

ADD ./instantclient_19_3.tar.gz ./arm_MorphoSmart_SDK_6.22.0.0_linux.tar.gz ./jdk-8u202-linux-arm32-vfp-hflt.tar.gz ./nasm-2.14.02.tar.gz ./yaml-cpp-yaml-cpp-0.6.0.tar.gz ./libjpeg-turbo-1.5.2.tar.gz ./poco-1.9.0-all_thangnq.tar.gz ./SDL2-2.0.7.tar.gz ./SDL2_image-2.0.2.tar.gz /home/vietic/myspace/

WORKDIR /home/vietic/myspace

RUN cd ./MorphoSmart_SDK_6.22.0.0_linux/PC/Sdk_linux && make install && cd ../../.. && rm -Rf MorphoSmart_SDK_6.22.0.0_linux && cd nasm-2.14.02 && ./configure && make && make install && cd .. && rm -Rf nasm-2.14.02 && cd yaml-cpp-yaml-cpp-0.6.0 && mkdir build && cd build && cmake -DYAML_CPP_BUILD_TESTS=OFF .. && make && make install && cd ../.. && rm -Rf yaml-cpp-yaml-cpp-0.6.0 && cd libjpeg-turbo-1.5.2 && ./configure --prefix=/usr --mandir=/usr/share/man --with-jpeg8 --disable-static --docdir=/usr/share/doc/libjpeg-turbo && make && make install && cd .. && rm -Rf libjpeg-turbo-1.5.2

RUN git clone http://git.videolan.org/git/ffmpeg.git && git clone https://git.videolan.org/git/ffmpeg/nv-codec-headers.git && cd nv-codec-headers && make install && cd .. && rm -Rf nv-codec-headers && cd ffmpeg && ./configure --enable-gpl --enable-libx264 --enable-libx265 --enable-libmp3lame --enable-libfdk-aac --enable-libopus --enable-pic --enable-shared --disable-static --extra-cflags=-fPIC --extra-ldexeflags=-pie --enable-pthreads --enable-nonfree --extra-cflags=-I/usr/include && make && make install && ldconfig && cd .. && rm -Rf ffmpeg

RUN git clone https://github.com/opencv/opencv.git && git clone https://github.com/opencv/opencv_contrib.git && cd opencv && mkdir release && cd release && cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local -D ENABLE_FAST_MATH=1 -D INSTALL_C_EXAMPLES=OFF -D WITH_GTK=ON -D ENABLE_PRECOMPILED_HEADERS=OFF -D BUILD_EXAMPLES=OFF -D BUILD_NEW_PYTHON_SUPPORT=ON -D BUILD_opencv_python3=ON -D HAVE_opencv_python3=ON -D PYTHON_DEFAULT_EXECUTABLE=/usr/bin/python3 -DOPENCV_EXTRA_MODULES_PATH=../../opencv_contrib/modules .. && make && make install && cd ../.. && rm -Rf opencv && rm -Rf opencv_contrib

RUN apt-get install -y python3-dev python3-mysqldb python3-tk python3-pip python3-numpy python3-sklearn python3-sklearn-lib python3-skimage python3-pandas && pip3 install --upgrade pip Cython && pip install --upgrade pandas requests pika psutil regex future joblib Flask Flask-HTTPAuth flask_cors unidecode redis six cryptography Jinja2 Werkzeug MarkupSafe pycryptodomex colored peewee pytz click itsdangerous guizero uwsgi zeep cherrypy cx_Oracle

RUN apt-get install -y mariadb-client && cd poco-1.9.0-all && ./configure --no-tests --no-samples && make && make install && cd .. && rm -Rf poco-1.9.0-all
# RUN cd SDL2-2.0.7 && ./configure --host=arm-raspberry-linux-gnueabihf --prefix=/usr && make && make install && cd .. && rm -Rf SDL2-2.0.7 && cd SDL2_image-2.0.2 && ./configure && make && make install && cd .. && rm -Rf SDL2_image-2.0.2


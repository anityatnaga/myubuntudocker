FROM ubuntu:latest
MAINTAINER fptai

# Set the timezone.
# ENV TZ=Asia/Ho_Chi_Minh
# RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get clean && apt-get -y update && apt-get install -y apt-utils locales
ENV LANG=en_US.UTF-8 LANGUAGE=en_US:en LC_CTYPE="en_US.UTF-8" LC_NUMERIC=en_US.UTF-8 LC_TIME=en_US.UTF-8 LC_COLLATE="en_US.UTF-8" LC_MONETARY=en_US.UTF-8 LC_MESSAGES="en_US.UTF-8" LC_PAPER=en_US.UTF-8 LC_NAME=en_US.UTF-8 LC_ADDRESS=en_US.UTF-8 LC_TELEPHONE=en_US.UTF-8 LC_MEASUREMENT=en_US.UTF-8 LC_IDENTIFICATION=en_US.UTF-8 LC_ALL=en_US.UTF-8 LD_LIBRARY_PATH=/home/fptai/myspace/myworks:/home/fptai/myspace/fpt-ai/ai-engine:/usr/local/lib KERAS_BACKEND=tensorflow FPTAI_CONFIG=./fptai.conf
RUN locale-gen en_US.UTF-8

RUN apt-get update && apt-get install -y sudo mc nano build-essential cmake git autoconf yasm curl wget checkinstall openssl libssl-dev libiodbc2 libiodbc2-dev libmysqlclient-dev software-properties-common libxext-dev libxrender-dev libxtst-dev rabbitmq-server

RUN mkdir /home/fptai && mkdir /home/fptai/myspace && mkdir /home/fptai/myspace/myworks && mkdir /home/fptai/myspace/fpt-ai && echo "root:FTI123$%^" | chpasswd

RUN apt-get install -y python3-dev python3-pip python3-mysqldb libmecab-dev mecab mecab-ipadic-utf8 && pip3 install --upgrade pip && pip install keras nltk pandas Cython requests sklearn boto3 pika bottle tensorflow cherrypy mecab-python3 h5py pyvi

COPY ./get_nltk_data.py /home/fptai/myspace/
RUN chmod +x /home/fptai/myspace/get_nltk_data.py && python3 /home/fptai/myspace/get_nltk_data.py

WORKDIR /home/fptai/myspace
RUN wget https://github.com/downloads/chokkan/liblbfgs/liblbfgs-1.10.tar.gz && tar xzvf liblbfgs-1.10.tar.gz && cd /home/fptai/myspace/liblbfgs-1.10 && ./configure && make && make install && cd /home/fptai/myspace && rm -f ./liblbfgs-1.10.tar.gz && rm -Rf ./liblbfgs-1.10 && wget https://github.com/downloads/chokkan/crfsuite/crfsuite-0.12.tar.gz && tar xzvf crfsuite-0.12.tar.gz && cd /home/fptai/myspace/crfsuite-0.12 && ./configure && make && make install && rm -f /home/fptai/myspace/crfsuite-0.12.tar.gz && cd /home/fptai/myspace && rm -Rf ./crfsuite-0.12

# ADD ./jdk-8u131-linux-x64.tar.gz ./JVNpro.tar.gz /home/fptai/myspace/

## install java
# RUN update-alternatives --install "/usr/bin/java" "java" "/home/fptai/myspace/jdk1.8.0_131/bin/java" 1 && update-alternatives --install "/usr/bin/javac" "javac" "/home/fptai/myspace/jdk1.8.0_131/bin/javac" 1 && update-alternatives --install "/usr/bin/javah" "javah" "/home/fptai/myspace/jdk1.8.0_131/bin/javah" 1

#ADD ./predict0725.tar.gz /home/fptai/myspace/fpt-ai
#WORKDIR /home/fptai/myspace/fpt-ai/ai-engine
#CMD ["./start.sh"]

########################################################################################################
# sudo docker build -t tnaga/myubuntu:ufptai09 . --force-rm=true
# docker run -it --rm -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix -v /home/tnaga/myspace/myworks:/home/fptai/myspace/myworks -v /etc/localtime:/etc/localtime:ro --device /dev/snd --device /dev/dri --device /dev/video0 tnaga/myubuntu:vfptai06 /bin/bash
